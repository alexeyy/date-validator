# Building
docker build -t date-validator .

# Running
docker run -it --rm --name running-date-validator date-validator

# Run tests (there is actually a very low coverage but it's intended to grasp the most important date-logic issues)
docker run -it --rm --name running-date-validator date-validator vendor/phpunit/phpunit/phpunit --bootstrap vendor/autoload.php test

# Global TODOs
I had too little time to implement:
 + "Builder" approach for creating the validator;
 + a nice pool for validation errors;
 + better test coverage.

# The task
Create a String Validator, which uses one or more Validation Rules.
Create at least 2 validation rules:
● The first rule checks if string is populated at all. ( to check mandatory fields )
● The second rule checks if provided string is a valid historical date ( but only, if the date is provided ,as
the rule should be able to work with non-mandatory fields )
The Validator will expect to validate the string in DD/MM/YYYY format.
Any non-valid strings should be rejected. If validation fails, the reasons should be provided as user friendly error messages.
Test your Validator thoroughly before completing the task.
An example of the Validation use:
$dateString = '/03/12/1999';
// $validationRules - contains a collection of 2 described above validation rules
$validator = new StringValidator($validationRules);
$result = validator->validate($dateString);
if (!$result->isValid()) {
    $errorMessages = $result->getMessages();
}