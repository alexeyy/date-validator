<?php

require 'vendor/autoload.php';

echo 'Please type a string: ' . PHP_EOL;
$string = rtrim(fgets(STDIN));

$validator = new \Validator\Validator([
    new \Validator\Rule\NotEmpty(),
    new \Validator\Rule\CorrectDate(),
]);

$validationResult = $validator->validate(new \Validator\StringToValidate($string));

if ($validationResult->isValid()) {
    echo 'The string is valid, congrats.' . PHP_EOL;
} else {
    echo 'Sorry, the string you provided is not a valid date, here are some errors: ' . PHP_EOL;
    foreach ($validationResult->getErrors() as $errorNumber => $error) {
        $errorFormatted = sprintf('%d. %s' . PHP_EOL, $errorNumber + 1, $error);
        fwrite(STDERR, $errorFormatted);
    }
}
