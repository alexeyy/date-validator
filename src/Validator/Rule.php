<?php

namespace Validator;

interface Rule
{
    public function validate(StringToValidate $string, array &$errors): bool;
}
