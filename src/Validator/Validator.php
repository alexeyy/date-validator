<?php

namespace Validator;

class Validator
{
    /**
     * @var Rule[]
     */
    private $rules;

    /**
     * @param Rule[] $rules
     */
    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    public function validate(StringToValidate $sting): ValidationResult
    {
        $isStringValid = true;

        $errors = [];

        foreach ($this->rules as $rule) {
            if (!$rule->validate($sting, $errors)) {
                $isStringValid = false;
                break;
            }
        }

        return new ValidationResult($isStringValid, $errors);
    }
}
