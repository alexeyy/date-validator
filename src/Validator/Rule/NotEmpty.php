<?php

namespace Validator\Rule;

use Validator\Rule;
use Validator\StringToValidate;

class NotEmpty implements Rule
{
    public function validate(StringToValidate $string, array &$errors): bool
    {
        $isStringEmpty = (string)$string === '';
        if ($isStringEmpty) {
            $errors[] = 'The string is empty';
        }
        return !$isStringEmpty;
    }
}
