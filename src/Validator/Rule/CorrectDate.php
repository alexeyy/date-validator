<?php

namespace Validator\Rule;

use Validator\Rule;
use Validator\StringToValidate;

class CorrectDate implements Rule
{
    private static $format = 'd/m/Y';

    public function validate(StringToValidate $dateCandidate, array &$errors): bool
    {
        $datetime = \DateTime::createFromFormat(self::$format, $dateCandidate);
        $isCorrectDate = $datetime && $datetime->format(self::$format) == $dateCandidate;
        if (!$isCorrectDate) {
            $errors[] = 'The string is not a correct date';
        }
        return $isCorrectDate;
    }
}
