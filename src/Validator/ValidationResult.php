<?php

namespace Validator;

class ValidationResult
{
    /**
     * @var bool
     */
    private $isStringValid;

    /**
     * @var string[]
     */
    private $errors;

    /**
     * ValidatorResponse constructor.
     * @param bool $isStringValid
     * @param string[] $errors
     */
    public function __construct(bool $isStringValid, array $errors = [])
    {
        $this->isStringValid = $isStringValid;
        $this->errors = $errors;
    }

    public function isValid(): bool
    {
        return $this->isStringValid;
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
