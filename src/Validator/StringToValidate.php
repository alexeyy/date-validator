<?php

namespace Validator;

/**
 * ValueObject representing the string which is being validated
 */
class StringToValidate
{
    private $stringValue;

    public function __construct(string $stringValue)
    {
        $this->stringValue = $stringValue;
    }

    public function __toString(): string
    {
        return $this->stringValue;
    }
}
