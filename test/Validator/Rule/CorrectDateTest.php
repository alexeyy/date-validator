<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class CorrectDateTest extends TestCase
{
    public function testValidationSimpleDate(): void
    {
        $rule = new \Validator\Rule\CorrectDate();
        $errors = [];
        $result = $rule->validate(new \Validator\StringToValidate('21/03/2017'), $errors);
        $this->assertTrue($result);
    }

    public function testNotExistingCalendarDaysAreNotValid(): void
    {
        $rule = new \Validator\Rule\CorrectDate();
        $errors = [];
        $result = $rule->validate(new \Validator\StringToValidate('33/01/2017'), $errors);
        $this->assertFalse($result);
    }

    public function testFebruaryInLeapYear(): void
    {
        $rule = new \Validator\Rule\CorrectDate();
        $errors = [];
        // 2016 was a leap year
        $result = $rule->validate(new \Validator\StringToValidate('29/02/2016'), $errors);
        $this->assertTrue($result);
    }

    public function testFebruaryInNotLeapYear(): void
    {
        $rule = new \Validator\Rule\CorrectDate();
        $errors = [];
        // 2017 wasn't a leap year
        $result = $rule->validate(new \Validator\StringToValidate('29/02/2017'), $errors);
        $this->assertFalse($result);
    }
}